//
//  main.m
//  day8-git
//
//  Created by Dave Henson on 3/20/17.
//  Copyright © 2017 Dave Henson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
