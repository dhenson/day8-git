//
//  ViewController.h
//  day8-git
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(NSInteger)demo1;
-(NSInteger)demo2;

@end

