//
//  AppDelegate.h
//  day8-git
//
//  Created by Dave Henson on 3/20/17.
//  Copyright © 2017 Dave Henson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

